#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       cresmoi2.py
#       
#       Copyright 2013 Daniele Raimondi <eddiewrc@alice.it>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from sources.xhresmoiSettings import *
#import cloudConnected3_api as cloud
import time, re, os, sys, marshal
import sources.utilsPred as utils
import sources.xhresmoiPred_api as API
#from sources.buildSeqProfilesDB import buildSeqProfilesDB
from sources.buildSeqProfilesDB import buildSeqProfilesDB
from sources.grammarBlues2 import grammarBlues_pred
HHBLITS=True
#USAGE: python cresmoi2.py dataset window "set:s:4:train:ing" "tes:t:set:s"

WRITE_PREDS= False

def main():
	if len(sys.argv) != 3 or "help" in sys.argv[1]:	
		print "\nERROR: wrong input from command line. Please use the following format:\n"	
		print "USAGE:	python xhresmoi.py --msa TRIMMED_MSA_FILE\n"
		return 1
	seqFiles =sys.argv[1]
	window = WINDOW_SIZE
	modelSvm = MODEL_SVM
	modelHmm = MODEL_HMM	
	msa = sys.argv[2]
	
	
	print "Start prediction!"
	cresmoiPredict(seqFiles, window, modelSvm, modelHmm, msa)

def cresmoiPredict(seqFiles, window, modelSvm, modelHmm, msa):
	print "Window size: " + str(window)
	sys.stdout.flush()
	if window % 2 == 1:
		window -= 1		
	print "Computing the MSA profile..."
	freqdb, seq = buildSeqProfilesDB(msa)		
	sys.stdout.flush()	
	X, corresp = API.buildVectorsSingle(seq, window, freqdb)
	print "Checking vector dimensionality:  ",
	print "Num of dimensions %d" % len(X[0])
	sys.stdout.flush()
	print "Checking vectors integrity...",
	for i in X:
		if len(i) != len(X[0]):
			raise Exception(" ##### Wrong num of dimensions targets:" +str(( len(i),len(X[0]))))	
	print "OK!"	
	sys.stdout.flush()	
	import cPickle
	print "Reading the model...",
	model = cPickle.load(open(modelSvm))	
	print "Done."	
	sys.stdout.flush()	
	y = API.decision_function(model, X)	
	sys.stdout.flush()	
	print "Applying HMM correction..."
	hy, hCorr = grammarBlues_pred(y, corresp, modelHmm) 
	print "Predictions finished!"
	sys.stdout.flush()	
	if True:
		writeResults(hy, hCorr, corresp, msa, window)		
	return 0

def writeResults(label, hcorr, corresp, targetName, window):
	fileName = "./PREDICTIONS/xhresmoiPreds-"+targetName.split("/")[-1]+"_ws"+str(window+1)+".xh"
	os.system("mkdir -p ./PREDICTIONS")
	ofp = open(fileName+"-.out","wb")
	toMarsh=[]
	i = 0
	ofp.write("Xhresmoi 1.0 predictions. \nWritten by D. Raimondi <daniele.raimondi@vub.ac.be>\nPOSITION IN THE SEQUENCE\tPREDICTION (1 == oxidised, 0 == reduced)\n")
	assert len(label) == len(corresp)
	while i < len(label):
		ofp.write(str(corresp[hcorr[i]][1])+"\t"+str(label[i])+"\n")
		toMarsh.append((corresp[hcorr[i]],label[i]))
		i += 1
	ofp.close()
	#marshal.dump(toMarsh, open(fileName+"-.m","wb"),2)
	#marshalledPredictions = [((UID, POS) : PREDICTIONS),...]
	print "\nResults stored in %s " % fileName,
	#print " and marshalled in %s in the form (label[], corresp[])." % "cresmoiPrediction.m"
	return

if __name__ == '__main__':
	main()
