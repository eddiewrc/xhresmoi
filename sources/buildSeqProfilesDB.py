
import os

		
def readMSA(path):
	ifp = open(path, "rb")
	msa = ifp.readlines()
	#msa.pop(0) #drop the original seq?
	if verbosity >= 1:
		print "Read %d sequences." % len(msa)
	i = 0
	while i < len(msa):
		msa[i] = msa[i].strip()
		i += 1	
	return msa		
		
	
# SENZA GAP
def getFreqs(l):		
		lng = len(l[0])
		#print lng
		table = []
		i = 0
		while i < lng:
			tmp = {}
			#print i
			#a = 0
			for j in l:
				#a += 1
				tmp[j[i]] = tmp.get(j[i], 0) + 1 #sparse memorization! key not found excs are possible!
			#print a
			table.append(tmp)			
			if "-" in tmp.keys():
				del tmp["-"]
			tot = sum(tmp.values())
			for j in tmp.items():
				tmp[j[0]] = tmp[j[0]] / float(tot)			
			i += 1
		if verbosity >= 3:
			print "Calculated frequencies for %d positions." % (len(table))			
		return table
'''
from  newuPssm  import NewuPssm

def buildPssmDB (nomefile):	
	seqs=readMSA(nomefile)
	a = NewuPssm(seqs)
	table=a.pssm	
	tables[UID]=table
	return tables
'''

def buildSeqProfilesDB (nomefile):	
	seqs=readMSA(nomefile)		
	table=getFreqs(seqs)		
	return table, seqs[0]
	
#nomedir="castrenseAlign"
verbosity=1

#fromPSSMdir2table (nomedir)

def main():
	freqdb = buildPssmDB("alignmentCys/"+chosen_msa)
	return 0


if __name__ == '__main__':
	main()
