
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import os
import numpy as np
verbosity = 0
THRESHOLD = 0.001
def readFASTA(seqFile):	
	ifp = open(seqFile, "r")
	listaseq=[]		
	for line in ifp:
		listaseq.append(line.rstrip())		
	return listaseq

def getHenikoff(msa):
	lng = len(msa[0]) #assuming that the first is the query seq
	table = []
	i = 0
	while i < lng:
		tmp = {}		
		for seq in msa:
			tmp[seq[i]] = tmp.get(seq[i], 0) + 1 #sparse memorization! key not found excs are possible!			
		if "-" in tmp.keys():
			del tmp["-"]
		if len(tmp.keys()) > 0:
			conserv = 1.0 / float(len(tmp.keys()))
			#tot = sum(tmp.values())
			for j in tmp.items():
				tmp[j[0]] = conserv / float(j[1])
		table.append(tmp)		
		i += 1
	i = 0
	#print lng
	filtered = []	
	while i < len(msa):
		filtered.append(0.0)
		i+=1
	#print msa[0][7]
	j = 0
	i = 0
	while i < lng:
		#print "esterno: %d" %i
		j = 0
		while j < len(msa):
			#print j
			#print msa[j][i]			
			if msa[j][i] != "-":				
				#print "ecco " +  str( table[i][msa[j][i]])
				filtered[j] += table[i][msa[j][i]]			
			j += 1
		i +=1
	#print len(msa)	
	#print len(filtered)
	tot = sum(filtered)
	
	i = 0
	while i < len(msa):
		filtered[i] = filtered[i] / float(tot)
		i += 1
		#print seq / float(tot) ,
		#print seq
	'''
	import matplotlib.pyplot as plt
	plt.xlabel('Weight')
	plt.ylabel("Frequency")
	plt.title("MSA")
	plt.hist(filtered, len(filtered)/5, facecolor='r')		
	plt.grid(True)
	plt.show()	
	'''
	if verbosity >= 3:
		print "Calculated frequencies for %d positions." % (len(table))		
	filteredMSA = []
	i = 0
	if len(msa) < 10: #henikoff 1 ha thr + alta nell else commentato
		filteredMSA = msa
	else:
		mean = np.mean(filtered)
		std = np.std(filtered)	
		while i < len(msa):
			if (filtered[i] - mean) / float(std) > -2.0*std: #questa furbata essere henikoff 2 (melgio usare il due)
				filteredMSA.append(msa[i])
			i += 1
	'''
	else: #henikoff standard
		while i < len(msa):
			if filtered[i] > THRESHOLD:
				filteredMSA.append(msa[i])
			i += 1
	'''
	return filteredMSA

def getFreqs(msa):		
		lng = len(msa[0]) #assuming that the first is the query seq
		#print lng
		table = []
		i = 0
		while i < lng:
			tmp = {}
			for seq in msa:
				tmp[seq[i]] = tmp.get(seq[i], 0) + 1 #sparse memorization! key not found excs are possible!			
			if "-" in tmp.keys():
				del tmp["-"]
			tot = sum(tmp.values())
			for j in tmp.items():
				tmp[j[0]] = tmp[j[0]] / float(tot)
			table.append(tmp)
			i += 1
		if verbosity >= 3:
			print "Calculated frequencies for %d positions." % (len(table))			
		return table

def buildSeqProfilesDB (nomedir):
	listafiles=os.listdir(nomedir)
	tables={}
	for nomefile in listafiles:
		#print nomefile
		UID=nomefile[:6]
		if verbosity >= 4:	
			print UID
		seqs=readFASTA(nomedir+"/"+nomefile)	
		#table=getFreqs(seqs)
		table=getFreqs(getHenikoff(seqs))
		tables[UID]=table
	return tables

def main():
	buildSeqProfilesDB ("../alignmentCys/SSsequenze1iterE-4AlignDir")
	seqs=readFASTA("../alignmentCys/SSsequenze1iterE-4AlignDir/"+"Q57VC7_iter1_eval0.0001_.hh")	
	table=getFreqs(getHenikoff(seqs))
	#table=getHenikoff(seqs)
	#for i in table:
	#	print i
	return 0

if __name__ == '__main__':
	main()

