#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       cresmoi_api.py
#       
#       Copyright 2013 Daniele Raimondi <eddiewrc@alice.it>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
from xhresmoiSettings import *
import numpy as np
import copy

def train(X,Y):
	#from sklearn.ensemble import RandomForestClassifier
	#learner = RandomForestClassifier(n_estimators=100, max_depth=None, min_samples_split=50, n_jobs=2)
	#from sources.RandomForest import RandomForest
	#learner = RandomForest()
	#from sklearn.ensemble import ExtraTreesClassifier
	#learner = ExtraTreesClassifier(n_estimators=150, max_depth=None, min_samples_split=50, n_jobs=3)
	#from sklearn.svm import SVC
	##learner = SVR(kernel='rbf', degree=3, gamma=0.0, coef0=0.0, tol=0.001, C=1.0, epsilon=0.1, shrinking=True, probability=False, cache_size=2000, verbose=False)
	#learner = SVC(C=1.0, cache_size=4000, class_weight=None, coef0=0.0, degree=3,gamma=0.0, kernel='rbf', probability=False,shrinking=True, tol=0.001, verbose=False)
	#learner = svm.LinearSVC(penalty='l2', loss='l2', dual=False, tol=0.0001, C=1.0, multi_class='ovr', fit_intercept=True, intercept_scaling=1, class_weight={1:1.2,0:1}, verbose=0)
	#from sources.ShogunMKL import  ShogunMKL
	#learner = ShogunMKL(REGRESSION=False)
	from sources.ShogunSVM import ShogunSVM
	learner = ShogunSVM(REGRESSION=False) #regression appears to be worse
	learner.fit(X, Y)
	if STORE_MODEL == True:
		print "Dumping model...",
		import cPickle
		cPickle.dump(learner, open("MODEL","wb"))		
		print "Done!"
	return learner

def predict(learner, X):
	y = learner.predict(X)
	return y

def decision_function(learner, X):
	y = learner.decision_function(X)
	return y	

def buildVectorsSingle(seq, w, pssmDB):# db = {seq, [cyspos1, cyspos2, ...]}
	X = []	
	corresp = []	
	#tmp = copy.deepcopy(prot[1][1])	
	i = 0
	cysList = []
	while i < len(seq):		
		if seq[i] == "C":
			cysList.append(i)
		i+=1
	for pos in cysList:
		if seq[pos] != "C":
			raise Exception("ERROR: Cysteine not centered in: ",seq[pos], pos)			
		
		########################################################
		X.append(freqdb2vec(w, pos, pssmDB))
		########################################################				
		corresp.append(("", pos))
	assert len(X) == len(corresp)	
	print "Build %d vectors" % len(X)
	return X, corresp


def buildVectors(db, w, pssmDB):# db = {seq, [cyspos1, cyspos2, ...]}
	X = []	
	corresp = []
	for prot in db.items(): # db = {UID: [seq, [cyspos1, cyspos2, ...]]}	
		if not pssmDB.has_key(prot[0]):
			print " ## Skipping %s, not found!" % prot[0]
			continue	
		tmp = copy.deepcopy(prot[1][1])			
		for pos in tmp:
			if prot[1][0][pos] != "C":
				raise Exception("ERROR: Cysteine not centered in: "+str((prot[0])))			
			
			########################################################
			#MODIFY ENCODING SCHEME HERE!!!
			########################################################
			#X.append(PSSM2vect(prot[0], w, pos, pssmDB)+[isSecreted(prot[0],secretedDB)])
			#X.append(dynaMine2vect(prot[0], w, pos, dynaMineDB)+secStruct2Vect(prot[0], pos, secStructDB))
			X.append(freqdb2vec(prot[0], w, pos, pssmDB))#+[isSecreted(prot[0],secretedDB)]
			#X.append(freqdb2vec(prot[0], w, pos, pssmDB)+aa_freqs(prot[1]))
			#X.append(freqdb2vec(prot[0], w, pos, pssmDB)+rsa2vec(prot[0], w, pos, rsaDB))
			#X.append(freqdb2vec(prot[0], w, pos, pssmDB)+ss2vec(prot[0], 0, pos, ssDB))
			#X.append(freqdb2vec(prot[0], w, pos, pssmDB)+ss2vec(prot[0], w, pos, ssDB)+rsa2vec(prot[0], w, pos, rsaDB))
			#X.append(ss2vec(prot[0], w, pos, ssDB))
			#X.append(rsa2vec(prot[0], w, pos, rsaDB))
			#X.append(freqdb2vec(prot[0], w, pos, pssmDB)+dynaMine2vect(prot[0], w, pos, dynaMineDB))
			#X.append(freqdb2vec(prot[0], w, pos, pssmDB)+attF.KittyDoolittle(prot[1][1],pos))
			#X.append(freqdb2vec(prot[0], w, pos, pssmDB)+attF.longKittyDoolittle (prot[1][1],pos,w))
			#X.append(PSSM2vect(prot[0], w, pos, pssmDB))
			########################################################				
			corresp.append((prot[0], pos))
	assert len(X) == len(corresp)	
	print "Build %d vectors" % len(X)
	return X, corresp
	
def aa_freqs(seq):
	listAA=['A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y']
	flist = []
	for aa in listAA:
		flist.append(seq.count(aa)/float(len(seq)))
	return flist

##### SENZA GAP
def freqdb2vec(sw, pos, freqDB):
#freqDB e una lista di posizioni, contenenti ognuno un dizionario di freq	
	listAA=['A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y']
	vec=[]	
	startpos=pos-(sw/2)	
	if startpos < 0 :
		vec= (20 * [0.0]) * (-startpos)
		startpos=0
	endpos=pos+(sw/2)
	#valuto quanti vettori dovro' aggiungere alla fine
	if endpos > (len (freqDB)-1):
		finaladd= endpos - (len (freqDB)-1)
		endpos=len(freqDB)-1
	else:
		finaladd=0
	for i in range (startpos,(endpos+1)):
		for aa in listAA:		
			if aa in freqDB[i].keys():
				vec.append(freqDB[i][aa])
			else:
				vec.append(0.0)		
#aggiungo eventuali robe alla fine

	for j in range (0,finaladd):
		vec=vec+(20 * [0.0])	
	if  len(vec)!= 20*(sw+1):
		vec=[0.0]*20*(sw+1)		
	return vec

def PSSM2vect(uid, sw, pos, pssmDB):
	seqProf = pssmDB[uid]
	if seqProf == None:
		raise Exception("ERROR! PSSM of " + uid + " not found!")
	i = pos - sw/2
	r = []
	broken = False
	while i < 0:
		broken = True
		r += [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		i += 1
	while i <= pos + sw/2 and i < len(seqProf):
		r += seqProf[i][1]
		i += 1
	while i <= pos + sw/2:
		broken = True
		r += [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		i += 1

	return r

def label(s):
	#print s
	if s == "FREE":
		return 0
	elif s == "SSBOND":
		return 1
	elif s == "BOND":
		return 1
	else:
		raise Exception("ERROR: I want SSBOND or FREE here!")

def main():
	
	return 0






if __name__ == '__main__':
	main()

