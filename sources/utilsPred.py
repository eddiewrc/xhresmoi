#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       utility.py
#       
#       Copyright 2013 Daniele Raimondi <eddiewrc@alice.it>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
import sys
import time
import re
import copy
import math
from xhresmoiSettings import *

#  
def readFASTA(seqFile):
	ifp = open(seqFile, "r")
	sl = []
	i = 0
	line = ifp.readline()
	while len(line) != 0:
		if line[0] == '>':	
			#sl.append([line.strip().replace("|","").replace(">","")[:5]+"_",""])	#only for spx	
			sl.append([line.strip().replace("|","-").replace(">","")[:14],""]) #attenzione
			line = ifp.readline()
			while len(line) > 0 and line[0] != '>':
				sl[i][1] = sl[i][1] + line.strip()
				line = ifp.readline()
			i = i + 1
		else:
			raise Exception("Syntax error in the fasta file "+seqFile+"!")	
	return sl
	
def readPredDataset(seqFile):
	fa = readFASTA(seqFile)
	db = {}
	for i in fa:
		db[i[0]] = [i[1], getCysPos(i[1])]
	return db # db = {seq, [cyspos1, cyspos2, ...]}
	
def getCysPos(seq):
	pos = []
	i = 0
	while i < len(seq):	
		if seq[i] == "C":
			pos.append(i)
		i+=1
	return pos

def readSecreted(path):
	ifp = open(path)
	uids = ifp.readlines()
	secretedDB = {}
	for i in uids:
		secretedDB[i.replace("\n","")] = ""
	if verbosity >= 0:
		print "Found %d UIDs of secreted proteins!" % (len(secretedDB.keys()))
	return secretedDB



PSIBLAST_AA_ORDER = ['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S',
 'T', 'W', 'Y', 'V']
verbosity = 0

def readPSSM(fileName):
	#try:
	ifp = open(PSSM_DIR+fileName, "r")
	#except IOError as e:
	#	if verbosity >= 2:
	#		print "WARNING: " + PSSM_DIR+fileName + " not found, skipping!"
	#	return None
	nullRow = 0
	seqProf = []
	strings = ifp.readlines()
	tot = 0.0
	i = 3
	while i < len(strings):
		if re.search(r"\s+\d+\s(\w{1,1}).*", strings[i]) != None:
			#print i
			match = re.search(r"\s+\d+\s(\w{1,1})\s+.*", strings[i])
			#print strings[i][match.start(1):match.end(1)]
			amino = strings[i][match.start(1):match.end(1)]
			#print strings[i][70:]
			match = re.search(r"^\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+.*", strings[i][70:])
			j = 1
			l = []
			while j <= 20:
				l.append(int(strings[i][70+match.start(j):70+match.end(j)]))
				j += 1
			#print l			
			tot = sum(l)
			if tot == 0:
				nullRow += 1
			j = 0
			if tot != 0:
				while j < 20:			
					l[j] = l[j] / float(tot)
					#print l
					#print sum(l)
					j += 1
			seqProf.append((amino, l))
		i += 1
	ifp.close()
	if nullRow > 0 and verbosity >= 2:
		print "Have been found %d null rows (due to PSI-BLAST roundin')" % (nullRow)
	return seqProf	# [ (aminoacid, [0,0,...prob distro , 0 ,0, 0.15]) ]

def createPSSMsDB(l):
	skipped = 0
	PSSM_DB = {}
	#print "Num of UIDs: "+str(len(l))
	for i in l:		
		PSSM_DB[i] = readPSSM(i)
	print "Num of PSSMs not found: %d" % (skipped)
	return PSSM_DB

def parseMetaDataPred(mds, seq):
	#print mds
	s = mds.split(" ")
	assert seq[int(s[1])] == "C"
	return int(s[1])


def readPredFile(filenames):
	#list of databases
	training = {}
	totSeqs = 0
	totCys = 0
	start = time.time()
	longer = 0
	for g in filenames:
		ifp = open(g, "r")
		if verbosity >= 1:
			print "Reading sequences from %s..." % (g)			
		i = 0
		line = ifp.readline()
		while len(line) != 0:			
			if line[0] == '>':
				#print line
				uniProt =  line[1:7]

				if verbosity >= 2:
					print line
					print "uniProt: " + uniProt
				line = ifp.readline()
				seq = ""
				while line[0:3] != "CYS":
					seq += line
					line = ifp.readline()
				if verbosity >= 2:
					print "Seq: " + seq
				
				metaData = []
				while "CYS" in line:					
					metaData.append(parseMetaDataPred(line, seq))
					totCys += 1
					line = ifp.readline()
				if verbosity >= 2:
					print "Metadata: " + str(metaData)
				#cleansingXs part
				if training.has_key(uniProt):
					print "WARNING: protein: " + uniProt + " is already present in the database!"
				if len(metaData) < 100:					
					training[uniProt] = [seq.replace("\n",""), metaData]
					totSeqs += 1
				else:
					longer+=1
					print uniProt
				line = ifp.readline()
			#continue
	fine = time.time()
	print "Read %d cys" % totCys
	print "Missing proteins due to cysteine excess: %d" % longer
	print " Done: read %d seqs in %.3fs" % (len(training), (fine-start))
	#print training["Q8IV16"]
	#raw_input()
	return training # UID:(pdb, seq, [(free, n), (bond, n, n)])

def retrieveUID(l):
	uidList = []
	for i in l:
		ifp = open(i, "r")
		lines = ifp.readlines()
		for j in lines:
			uidList.append(j.replace("\n", ""))
		
	if verbosity >= 2:
		print "Num of uid retrieved: " + str(len(uidList))
	return uidList	



def main():
	
	return 0

if __name__ == '__main__':
	main()

