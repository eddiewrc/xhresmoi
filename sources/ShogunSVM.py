#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       ShogunSVM.py
#       
#       Copyright 2013 Daniele Raimondi <eddiewrc@alice.it>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

#from shogun.Features import *
#from shogun.Kernel import *
#from shogun.Classifier import *
#from shogun.Evaluation import *
from modshogun import *
from numpy import *
from numpy.random import randn

def correctLabels(Y):
	y = []
	for i in Y:
		if i > 0:
			y.append(1)
		else:
			y.append(-1)
	return array(y,dtype=float64)	

class ShogunCombSVM:
	
	def __init__(self, REGRESSION=False, gamma=1.0, C=1.0):
		self.REGRESSION=REGRESSION
		self.gamma=gamma
		self.svm=None
		self.kernel=None
		self.C = 1.0

	def fit(self, X, Y):
		print "Start training..."
		x = RealFeatures(array(X,dtype=float64).T);
		Yc = correctLabels(Y)
		if not self.REGRESSION:
			y = BinaryLabels(Yc);
		else:
			y = RegressionLabels(Yc)
		width = 2;	
		self.kernel = CombinedKernel()
		feats = CombinedFeatures()
		k1 = Chi2Kernel(x, x, 10, 100)
		#k1.init(x, x)
		self.kernel.append_kernel(CustomKernel(k1.get_kernel_matrix()))
		feats.append_feature_obj(x)
		k2 = GaussianKernel(1,5.0)			
		
		self.kernel.append_kernel(k2)			
		self.kernel.init(feats, feats)
		self.x = x
		self.fx = feats		
		if not self.REGRESSION:
			self.svm = LibSVM(self.C, self.kernel, y)
		else:
			self.svm=LibSVR(self.C, 0.1, self.kernel, y)
		self.svm.train();
		print "Done."
			
	def predict(self, X):
		assert self.svm != None and self.kernel != None and self.gamma != None
		print "Predicting..."
		x = RealFeatures(array(X,dtype=float64).T);
		
		
		kernel = CombinedKernel()
		feats_pred = CombinedFeatures()
		
		k1 = Chi2Kernel(x, x, 10, 100)
		k1.init(self.x, x)
		kernel.append_kernel(CustomKernel(k1.get_kernel_matrix()))

		feats_pred.append_feature_obj(x)
		k2 = GaussianKernel(1,5.0)
		kernel.append_kernel(k2)
		kernel.init(self.fx, feats_pred)

		self.svm.set_kernel(kernel)
		out = self.svm.apply()
		
		print "Done!"
		return out.get_labels()

	def decision_function(self, X):
		return self.predict(X)
		

class ShogunSVM:
	
	def __init__(self, REGRESSION=False, gamma=5, C=1.0):
		self.REGRESSION=REGRESSION
		self.gamma=gamma
		self.svm=None
		self.kernel=None
		self.C = 1.0

	

	def fit(self, X, Y):
		print "Start training..."
		x = RealFeatures(array(X,dtype=float64).T);
		Yc = correctLabels(Y)
		if not self.REGRESSION:
			y = BinaryLabels(Yc);
		else:
			y = RegressionLabels(Yc)
		width = 2;
		#self.kernel = GaussianKernel(x, x, self.gamma); #gamma 5
		#self.kernel = ANOVAKernel(x, x, 2, 10)
		#self.kernel = GaussianShiftKernel(x, x, self.gamma, 2, 1)#best
		############################AUCK
		#subkernel=GaussianKernel(x, x, self.gamma)
		#self.kernel=AUCKernel(0, subkernel)
		#self.kernel.setup_auc_maximization(y)
		#Y U NO WORK?
		#####################################		
		#km = kernel.get_kernel_matrix();
		##########CAUCHY	 #SEMBRA BUONO; INDAGA SUI PARAMETRI
		#distance=ManhattanMetric()
		#self.kernel=CauchyKernel(x, x, 73, distance)
		#######################################
		#self.kernel=Chi2Kernel(x, x, 10, 100) #BEST for single
		#distance=EuclideanDistance(x, x)
		#self.kernel=CircularKernel(x, x, 20, distance) NO
		#BrayCurtisDistance()#ChebyshewMetric()#GeodesicMetric()#JensenMetric()#ManhattanMetric()#MinkowskiMetric()#TanimotoDistance()#EuclideanDistance()
		#distance=MinkowskiMetric()
		#self.kernel=DistanceKernel(x, x, 10, distance)
		#distance = GeodesicMetric(x, x)
		#self.kernel=ExponentialKernel(x, x, 0.5, distance, 100)
		#distance=EuclideanDistance(x, x)
		#self.kernel=InverseMultiQuadricKernel(x, x, 10, distance)
		#distance=EuclideanDistance(x, x)
		#self.kernel=LogKernel(x, x, 7, distance)
		#distance=EuclideanDistance(x, x)
		#self.kernel=MultiquadricKernel(x, x, 17, distance)
		#distance=ChebyshewMetric(x, x)
		#self.kernel=PowerKernel(x, x, 13, distance)
		#distance=EuclideanDistance(x, x)
		#self.kernel=RationalQuadraticKernel(x, x, 2, distance)
		#self.kernel=SigmoidKernel(x, x, 10, 11.2, 1.3)
		##self.kernel=GaussianKernel()
		##self.kernel.set_normalizer(AvgDiagKernelNormalizer(2))
		##self.kernel.init(x, x)
		#distance=EuclideanDistance(x, x)
		#self.kernel=MultiquadricKernel(x, x, 1, distance)
		#self.kernel=SplineKernel(x, x)
		
		#distance = JensenMetric()
		#self.kernel=TStudentKernel(x, x, 1, distance) #provalo
		#distance=MinkowskiMetric()
		#self.kernel=WaveKernel(x, x, 15.2, distance)
		#self.kernel=WaveletKernel(x, x, 10, 1, 1.5)
		########################################COMBINED
		
		###best here
		#distance=ManhattanMetric()
		#self.kernel=CauchyKernel(x, x, 73, distance)
		self.kernel=Chi2Kernel(x, x, 10, 10) ######BEST for single
		
		#if not self.REGRESSION:
			#self.svm = GPBTSVM(self.C, self.kernel, y)
			#self.svm = MPDSVM(self.C, self.kernel, y)
			#self.svm.set_epsilon(1e-5)
		self.svm = LibSVM(self.C, self.kernel, y) #STANDARD
		self.svm.parallel.set_num_threads(4)
		#else:
		#	self.svm=LibSVR(self.C, 0.01, self.kernel, y)
		self.svm.train();
		print "Done."

	def predict(self, X):
		assert self.svm != None and self.kernel != None and self.gamma != None
		print "Predicting..."
		x = RealFeatures(array(X,dtype=float64).T);
		out = self.svm.apply(x)
		print "Done!"
		return out.get_labels()

	def decision_function(self, X):
		assert self.svm != None and self.kernel != None and self.gamma != None
		print "Predicting..."
		x = RealFeatures(array(X,dtype=float64).T);
		out = self.svm.apply_regression(x)
		print "Done!"
		return out.get_labels()

def main():
	ml = ShogunSVM(REGRESSION=False)
	x = [[0,0],[0,1],[2,0],[2,1]]
	print x
	y = [-1,-1,1,1]
	ml.fit(x,y)
	test = [[-1,0],[0,2],[5,0]]
	print test
	res = ml.predict(test)
	#pm = PerformanceMeasures(labels_test, output);
	#print pm.get_accuracy();
	#print  pm.get_auROC();
	#print pm.get_fmeasure();
	print res
	
	return 0

if __name__ == '__main__':
	main()



