#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it, daniele.raimondi@vub.ac.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys, os
from multiprocessing import Pool
SI=0.85
COV=0.80
verbosity = 0

def readMSA(alName, si, cov):
        ifp = open(alName, "rb")
        msa = ifp.readlines()
        ifp.close()
        if len(msa) == 0:
                return msa
        if verbosity >= 2:
                print " > Read %d sequences." % len(msa)
        i = 0
        while i < len(msa):
                msa[i] = msa[i].strip()
                i += 1
        
        #filter using seq ID
        query = msa.pop(0)
        msaFilt = [query]
        for i in msa:
                if getSeqIDsimilarity(query,i) > si and coverage(query, i) > cov:
                        msaFilt.append(i)       
        if verbosity >= 2:
                print "Filtered %d sequences." % len(msaFilt)   
        #if len(msaFilt) > 100:
        #       print "***TOO MANY SEQUENCES "
        #       return msaFilt[:100]
        return msaFilt

def coverage(s1, s2):
	return len(s2.replace("-",""))/float(len(s1.replace("-","")))

def getSeqIDsimilarity(s1,s2): #change it with your favourite similarity measure!
	score = 0
	i = 0 
	while i < min(len(s1), len(s2)):
		if s1[i] == s2[i] and s1[i] != "-":
			score += 1
		i += 1
	return score/float(len(s2)-s2.count("-")) 

        
def buildFasta(msa, filename):
        tmpName = filename
        ofp = open(tmpName, "w")
        for i in msa:
                ofp.write(i+"\n")
        ofp.close()

def main():
	assert len(sys.argv) == 2	
	dirPath = sys.argv[1]
	l = os.listdir(dirPath)
	print l
	raw_input()
	i = 0
	
	pool = Pool(processes=20)
	for f in l:
		pool.apply_async(ex, args = (i, len(l), dirPath, f, SI, COV)) #####.get()
		i+=1
	pool.close()
	pool.join()		
	
	return 0
	
def ex(i, last, dirPath, f, si, cov):
	print "%d/%d" % (i,last)
	msa = readMSA(dirPath+f, SI, COV)
	buildFasta(msa, dirPath+f[:6]+".filt85SI80COV") #sp-Q6PKH6-DR_jh_eval0.1_.hmmer to P28827.filt85SI80COV
	

if __name__ == '__main__':
	main()

