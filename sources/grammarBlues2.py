#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       senza nome.py
#       
#       Copyright 2013 Daniele Raimondi <eddiewrc@alice.it>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
from xhresmoiSettings import *
import math
import sys
import re
import copy
verbosity = 0
interpolate = True
HIST=False
#interpolate best num classes = 40
#histogram best num classes = ?
classes = 40

def metadata2seq(m):
	l = []
	for i in m:		
		if i[0] == "SSBOND":
			#print i
			l.append(("b",i[1]))
			l.append(("b",i[2]))
		elif i[0] == "FREE":
			l.append(("f",i[1]))
		else:
			raise Exception("ERROR: I expect FREE/SSBOND here,instead of |"+str(i[0])+"|")
	seq = sorted(l, key=lambda value: value[1])
	if verbosity >= 3:
		print seq
	#l = ""
	#for i in seq:
	#	l += i[0]
	#print l
	return seq


def learnTransitions(train):
	#train HT of: (0UID, 1[0pdb, 1seq, 2metadata:[(free, n), (bond, n, n), ... ]])
	hmm = {"s":["f1","b1"], "f1":["f1","b1","e"], "f2":["f2","b2"], "b1":["f2","b2"], "b2":["f1","b1","e"]}
	transitions = {("s","f1"):0, ("s","b1"):0, ("f1","f1"):0, ("f1","b1"):0, ("f1","e"):0, ("b1","f2"):0, ("b1","b2"):0, ("f2","f2"):0, ("f2","b2"):0, ("b2","f1"):0, ("b2","b1"):0, ("b2","e"):0}
	states = {"s":0, "f1":0, "f2":0, "b1":0, "b2":0}
	
	st = ""
	for i in train.values():
		if verbosity >= 2:
			print i[0]
		seq = metadata2seq(i[2])		
		j = 0
		states["s"] += 1
		if seq[j][0] == "b":
			transitions[("s","b1")] += 1
			st = "b1"
			states["b1"] += 1
		elif seq[j][0] == "f":
			transitions[("s","f1")] += 1
			st = "f1"
			states["f1"] += 1
		j+=1
		while j < len(seq):
			a = True
			for h in hmm[st]:				
				if re.search(r"^"+seq[j][0]+"\d", h) != None:					
					#print st+h
					transitions[(st,h)] += 1
					st = copy.deepcopy(h)
					states[st] += 1
					j+=1
					break
		
		transitions[(st,"e")] += 1
		#states[st] += 1	
	
	if verbosity >= 2:
		#print st+"e"
		print hmm	
		print transitions
		print states
	for i in transitions.keys():
		for j in states.keys():
			if j == i[0]:
				#print j
				#print i
				transitions[i] = transitions[i] / float(states[j])
				break
	if verbosity >= 2:
		for i in states.keys():
			for j in transitions.items():
				if j[0][0] == i:
					print j
	return hmm, transitions

def getCysSequences(predTargetY, targetCorresp): #targetCorresp = [(uid,pos)]
	assert len(predTargetY) == len(targetCorresp)
	targetProts = []
	pred = {}
	i = 0
	while i < len(predTargetY): #organizes predictions per protein
		if not pred.has_key(targetCorresp[i][0]):
			pred[targetCorresp[i][0]] = []
		pred[targetCorresp[i][0]].append((targetCorresp[i][1], predTargetY[i], i)) # pos, prediction, numCys
		i += 1

	for i in pred.items(): #sorts predictions for each protein
		tmp = sorted(i[1],key=lambda x:x[0])
		#print pred[i[0]]
		pred[i[0]] = copy.deepcopy(tmp)
		#print pred[i[0]]
		tmp2 = []
		tmp3 = []
		for j in tmp:
			tmp2.append(j[1])
			tmp3.append(j[2])
		#print (tmp2,tmp3)
		targetProts.append((tmp2,tmp3))
	#print targetProts
	#raw_input()
	return targetProts #pred={uid:[(pos,pred,pos_in_targetCorresp),(pos,pred)]}

def printHIST(bond, free):	
	import matplotlib.pyplot as plt
	plt.xlabel('Classes')
	plt.ylabel("Frequency")
	#plt.title("Bond distro")
	plt.hist([free,bond],bins=50, color=['red','green'])
	#plt.hist(bond, facecolor='g')
	
	plt.grid(True)
	plt.savefig("bondDistros.png",ppi=400)
	plt.show()

def getHist(l):
	hist = {}
	for i in l:
		i *= classes
		n = round(i)
		hist[n] = math.fsum((hist.get(n,0),(1/float(len(l)))))	
	return hist

def learnEmissions(Y,y):
	free = []
	bond = []
	i = 0
	while len(Y) > i:
		#print (trData[0], float(trOut))
		if int(Y[i]) == 0:
			free.append(float(y[i]))
		elif int(Y[i]) == 1:
			bond.append(float(y[i]))
		else:
			raise Exception("ERROR: I must have 0/1 here!")
		i+=1
	############################################################
	if HIST == True:
		printHIST(bond, free) #THIS LINE PRINTS A DISTRO HISTOGRAM!
	############################################################
	bondHist = getHist(bond)
	freeHist = getHist(free)
	if verbosity >= 1:
		print "Bond Emission Distro:"
		for i in bondHist.items():
			print i
		print "Free Emission Distro:"
		for i in freeHist.items():
			print i
	#ht values*10:freq
	return bondHist, freeHist

def grammarBlues_pred(predTargetY, targetCorresp, model):
	print "Pickling model...",
	import cPickle
	data = cPickle.load(open(model))
	hmm = data[0]
	trans = data[1]
	em = data[2]
	print "Done."	
	protData = getCysSequences(predTargetY, targetCorresp)#corresp={uid:[(pos,pred,pos_in_targetCorresp),(pos,pred,pos_in_targetCorresp)]}
	response = []	
	for i in protData:
		#print i
		l = viterbi(i[0], hmm, trans, em)
		response.append((l,i[1]))
		assert (l.count("b1")+l.count("b2")) % 2 == 0
		#print l
		#print metadata2seq(dataBase[i[0]][2])
		#raw_input()	
	y = []
	corr = []
	for prot in response:
		#print prot
		i = 0
		while i < len(prot[0]):
			if prot[0][i][0] == 'b':
				y.append(1)
			else:
				y.append(0)
			corr.append(prot[1][i])
			i+=1
	return y, corr
	

def grammarBlues(trainSet, predictionSet, trainY, predTrainY, predTargetY, targetCorresp):	
	print "Learning transitions and emissions..."
	hmm, trans = learnTransitions(trainSet)
#hmm = {"s":["f1","b1"], "f1":["f1","b1","e"], "f2":["f2","b2"], "b1":["f2","b2"], "b2":["f1","b1","e"]}
#transitions = {("s","f1"):0, ("s","b1"):0, ("f1","f1"):0, ("f1","b1"):0, ("f1","e"):0, ("b1","f2"):0, ("b1","b2"):0, ("f2","f2"):0, ("f2","b2"):0, ("b2","f1"):0, ("b2","b1"):0, ("b2","e"):0}
	bondEM, freeEM = learnEmissions(trainY, predTrainY)
	protData = getCysSequences(predTargetY, targetCorresp)#corresp={uid:[(pos,pred,pos_in_targetCorresp),(pos,pred,pos_in_targetCorresp)]}
	response = []
	hmm = {"s":["f1","b1"],"f1":["f1","b1","e"], "f2":["f2","b2"], "b1":["f2","b2"], "b2":["f1","b1","e"]}
	import cPickle
	if STORE_MODEL == True:
		cPickle.dump((hmm, trans, {"b":bondEM, "f":freeEM}), open("correction.hmm","wb"))
	for i in protData:
		#print i
		l = viterbi(i[0], hmm, trans, {"b":bondEM, "f":freeEM})
		response.append((l,i[1]))
		assert (l.count("b1")+l.count("b2")) % 2 == 0
		#print l
		#print metadata2seq(dataBase[i[0]][2])
		#raw_input()	
	y = []
	corr = []
	for prot in response:
		#print prot
		i = 0
		while i < len(prot[0]):
			if prot[0][i][0] == 'b':
				y.append(1)
			else:
				y.append(0)
			corr.append(prot[1][i])
			i+=1
	return y, corr

def interpolate(distro, val):
	x1 = math.floor(val*classes)
	x2 = math.ceil(val*classes)
	if distro.has_key(x1):
		y1 = distro[x1]
	else:
		x1 -= 1
		while not distro.has_key(x1):
			x1 -= 1
		y1 = distro[x1]
	if distro.has_key(x2):
		y2 = distro[x2]
	else:
		x2 += 1
		while not distro.has_key(x2):
			x2 += 1		
		y2 = distro[x2]
	m = (y2-y1)/max(float(x2-x1),0.000000001)#############MODIFICA AFFRETTATA!!!!!!!!!!!!
	return ((val*classes -x1) * m) + y1

def outOfDistro(distro, val):
	if val < min(distro.keys()) or val > max(distro.keys()):
		return True
	else:
		return False

def possibleSt(hmm, k):
	l = []
	for i in hmm.items():
		if k in i[1]:
			l.append(i[0])
	return l

def viterbi(clist, hmm, trans, EM):	
	V = [{}]
	path = {}
	#init	
	for i in hmm.keys():
		V[0][i] = 0.0
		#path[i] = []	
	V[0]["s"] = 1.0	
	path["s"] = []
	#print V[0]
	#recursion
	#print "----------------"
	for i in range(1, len(clist)+1):
		#print "iter=%d----------------------------" %(i)
		V.append({})
		newpath = {}
		for k in hmm.keys():
			#print "k=%s" %(k)
			q = possibleSt(hmm,k)
			#print "q=%s" %(str(q))
			if len(q) == 0:
				V[i][k] = 0.0
			else:
				if not outOfDistro(EM[k[0]], clist[i-1]*classes):
				#if EM[k[0]].has_key(round(clist[i-1]*classes)):
					###################################################à
					#chose to use histogram or interpolated data
					if not interpolate:
						e = EM[k[0]][round(clist[i-1]*classes)]
					else:
						e = interpolate(EM[k[0]], clist[i-1])
					####################################################
				else:
					e = 0.0000000001 / (classes*10)
					#print "e =ZERO"
				l = [(V[i-1][y] * trans[(y, k)] * e, y) for y in q]
				#print l
				(p, st) = max(l, key=lambda v:v[0])				
				V[i][k] = p  
				#print "max=%s" %(str((p, st)))
				if p > 0.0:	
					newpath[k] = copy.deepcopy(path[st]) + [k]
					#print newpath[k]
		#print V[i]
		path = newpath
	#print "final round"
	#print "q=%s" %(str(possibleSt(hmm,"e")))
	l = []
	for y in ["f1", "b2"]:
		#print "k=%s" %(y)
		#print V[len(clist)]
		#print trans[(y,"e")]
		l += [(V[len(clist)][y] * trans[(y,"e")], y)]
		#print l
	(prob, st) = max(l, key=lambda v:v[0])
	#print path[st]
	if verbosity >= 2:
		Vtable(V,hmm.keys())	
	#raw_input()
	#print path[st]
	return path[st]

def Vtable(V, states):
	k = 0
	for i in V:
		print "t= %d" % k
		k += 1
		for j in states:
			print j + " " + str(i[j]) + " "	

def main():
	
	return 0

if __name__ == '__main__':
	main()

